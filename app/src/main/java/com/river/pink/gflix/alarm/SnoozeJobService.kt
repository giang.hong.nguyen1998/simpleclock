@file:Suppress("DEPRECATION")

package com.river.pink.gflix.alarm

import android.app.IntentService
import android.content.Intent
import com.simplemobiletools.commons.helpers.MINUTE_SECONDS

class SnoozeJobService : IntentService("Snooze") {

   @Deprecated("Deprecated in Java")
   override fun onHandleIntent(intent: Intent?) {
      val id = intent!!.getIntExtra(ALARM_ID, -1)
      val alarm = dbHelper.getAlarmWithId(id) ?: return
      hideNotification(id)
      setupAlarmClock(alarm, config.snoozeTime * MINUTE_SECONDS)
   }
}