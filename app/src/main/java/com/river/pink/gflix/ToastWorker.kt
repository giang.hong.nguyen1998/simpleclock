package com.river.pink.gflix

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ToastWorker(
   context: Context,
   params: WorkerParameters
) : CoroutineWorker(context, params) {

   override suspend fun doWork(): Result {
      Log.e("test", "ToastWorker")

      withContext(Dispatchers.Main) {
         Toast.makeText(applicationContext, "test worker in background", Toast.LENGTH_SHORT).show()
      }

      return Result.success()
   }
}