package com.river.pink.gflix

import android.annotation.SuppressLint
import android.app.KeyguardManager
import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.VibrationEffect
import android.os.Vibrator
import android.view.MotionEvent
import android.view.ViewGroup
import android.view.WindowManager
import android.view.animation.AnimationUtils
import androidx.core.view.WindowCompat
import com.river.pink.gflix.alarm.*
import com.river.pink.gflix.databinding.ActivityReminderBinding
import com.simplemobiletools.commons.extensions.*
import com.simplemobiletools.commons.helpers.*

class ReminderActivity : BaseSimpleActivity() {
   private val INCREASE_VOLUME_DELAY = 3000L

   private val increaseVolumeHandler = Handler()
   private val maxReminderDurationHandler = Handler()
   private val swipeGuideFadeHandler = Handler()
   private var isAlarmReminder = false
   private var didVibrate = false
   private var wasAlarmSnoozed = false
   private var alarm: Alarm? = null
   private var mediaPlayer: MediaPlayer? = null
   private var vibrator: Vibrator? = null
   private var lastVolumeValue = 0.1f
   private var dragDownX = 0f

   private lateinit var binding: ActivityReminderBinding

   override fun onCreate(savedInstanceState: Bundle?) {
      super.onCreate(savedInstanceState)

      binding = ActivityReminderBinding.inflate(layoutInflater)

      WindowCompat.setDecorFitsSystemWindows(window, false)

      setContentView(binding.root)
      showOverLockscreen()
      updateTextColors(binding.reminderHolder as ViewGroup)

      val id = intent.getIntExtra(ALARM_ID, -1)
      isAlarmReminder = id != -1
      if (id != -1) {
         alarm = dbHelper.getAlarmWithId(id) ?: return
      }

      val label = if (isAlarmReminder) {
         alarm!!.label.ifEmpty {
            getString(R.string.alarm)
         }
      } else {
         getString(R.string.timer)
      }

      binding.reminderTitle.text = label
      binding.reminderText.text = if (isAlarmReminder) getFormattedTime(
         getPassedSeconds(),
         false,
         makeAmPmSmaller = false
      ) else getString(R.string.time_expired)

      val maxDuration =
         if (isAlarmReminder) config.alarmMaxReminderSecs else config.timerMaxReminderSecs
      maxReminderDurationHandler.postDelayed({
         finishActivity()
      }, maxDuration * 1000L)

      setupButtons()
      setupEffects()
   }

   private fun setupButtons() {
      if (isAlarmReminder) {
         setupAlarmButtons()
      } else {
         setupTimerButtons()
      }
   }

   @SuppressLint("ClickableViewAccessibility")
   private fun setupAlarmButtons() {
      binding.reminderStop.beGone()
      binding.reminderDraggableBackground.startAnimation(
         AnimationUtils.loadAnimation(
            this,
            R.anim.pulsing_animation
         )
      )
      binding.reminderDraggableBackground.applyColorFilter(getProperPrimaryColor())

      val textColor = getProperTextColor()
      binding.reminderDismiss.applyColorFilter(textColor)
      binding.reminderDraggable.applyColorFilter(textColor)
      binding.reminderSnooze.applyColorFilter(textColor)

      var minDragX = 0f
      var maxDragX = 0f
      var initialDraggableX = 0f

      binding.reminderDismiss.onGlobalLayout {
         minDragX = binding.reminderSnooze.left.toFloat()
         maxDragX = binding.reminderDismiss.left.toFloat()
         initialDraggableX = binding.reminderDraggable.left.toFloat()
      }

      binding.reminderDraggable.setOnTouchListener { v, event ->
         when (event.action) {
            MotionEvent.ACTION_DOWN -> {
               dragDownX = event.x
               binding.reminderDraggableBackground.animate().alpha(0f)
            }
            MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
               dragDownX = 0f
               if (!didVibrate) {
                  binding.reminderDraggable.animate().x(initialDraggableX).withEndAction {
                     binding.reminderDraggableBackground.animate().alpha(0.2f)
                  }

                  binding.reminderGuide.animate().alpha(1f).start()
                  swipeGuideFadeHandler.removeCallbacksAndMessages(null)
                  swipeGuideFadeHandler.postDelayed({
                     binding.reminderGuide.animate().alpha(0f).start()
                  }, 2000L)
               }
            }
            MotionEvent.ACTION_MOVE -> {
               binding.reminderDraggable.x =
                  Math.min(maxDragX, Math.max(minDragX, event.rawX - dragDownX))
               if (binding.reminderDraggable.x >= maxDragX - 50f) {
                  if (!didVibrate) {
                     binding.reminderDraggable.performHapticFeedback()
                     didVibrate = true
                     finishActivity()
                  }

                  if (isOreoPlus()) {
                     notificationManager.cancelAll()
                  }
               } else if (binding.reminderDraggable.x <= minDragX + 50f) {
                  if (!didVibrate) {
                     binding.reminderDraggable.performHapticFeedback()
                     didVibrate = true
                     snoozeAlarm()
                  }

                  if (isOreoPlus()) {
                     notificationManager.cancelAll()
                  }
               }
            }
         }
         true
      }
   }

   private fun setupTimerButtons() {
      binding.reminderStop.background = resources.getColoredDrawableWithColor(
         R.drawable.circle_background_filled,
         getProperPrimaryColor()
      )
      arrayOf(
         binding.reminderSnooze,
         binding.reminderDraggableBackground,
         binding.reminderDraggable,
         binding.reminderDismiss
      ).forEach {
         it.beGone()
      }

      binding.reminderStop.setOnClickListener {
         finishActivity()
      }
   }

   private fun setupEffects() {
      if (!isAlarmReminder || !config.increaseVolumeGradually) {
         lastVolumeValue = 1f
      }

      val doVibrate = if (alarm != null) alarm!!.vibrate else config.timerVibrate
      if (doVibrate && isOreoPlus()) {
         val pattern = LongArray(2) { 500 }
         vibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
         vibrator?.vibrate(VibrationEffect.createWaveform(pattern, 0))
      }

      val soundUri =
         "android.resource://" + packageName + "/" + R.raw.default_alarm_alert_homecoming

      if (soundUri != SILENT) {
         try {
            mediaPlayer = MediaPlayer().apply {
               setAudioStreamType(AudioManager.STREAM_ALARM)
               setDataSource(this@ReminderActivity, Uri.parse(soundUri))
               setVolume(lastVolumeValue, lastVolumeValue)
               isLooping = true
               prepare()
               start()
            }

            if (config.increaseVolumeGradually) {
               scheduleVolumeIncrease()
            }
         } catch (_: Exception) {
         }
      }
   }

   private fun scheduleVolumeIncrease() {
      increaseVolumeHandler.postDelayed({
         lastVolumeValue = Math.min(lastVolumeValue + 0.1f, 1f)
         mediaPlayer?.setVolume(lastVolumeValue, lastVolumeValue)
         scheduleVolumeIncrease()
      }, INCREASE_VOLUME_DELAY)
   }

   override fun onNewIntent(intent: Intent?) {
      super.onNewIntent(intent)
      finishActivity()
   }

   override fun onDestroy() {
      super.onDestroy()
      increaseVolumeHandler.removeCallbacksAndMessages(null)
      maxReminderDurationHandler.removeCallbacksAndMessages(null)
      swipeGuideFadeHandler.removeCallbacksAndMessages(null)
      destroyEffects()
   }

   private fun destroyEffects() {
      mediaPlayer?.stop()
      mediaPlayer?.release()
      mediaPlayer = null
      vibrator?.cancel()
      vibrator = null
   }

   private fun snoozeAlarm() {
      destroyEffects()
      if (config.useSameSnooze) {
         setupAlarmClock(alarm!!, config.snoozeTime * MINUTE_SECONDS)
         wasAlarmSnoozed = true
         finishActivity()
      } else {
         showPickSecondsDialog(
            config.snoozeTime * MINUTE_SECONDS,
            true,
            cancelCallback = { finishActivity() }) {
            config.snoozeTime = it / MINUTE_SECONDS
            setupAlarmClock(alarm!!, it)
            wasAlarmSnoozed = true
            finishActivity()
         }
      }
   }

   private fun finishActivity() {
      if (!wasAlarmSnoozed && alarm != null && alarm!!.days > 0) {
         scheduleNextAlarm(alarm!!, false)
      }

      destroyEffects()
      finish()
      overridePendingTransition(0, 0)
   }

   private fun showOverLockscreen() {
      window.addFlags(
         WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON or
              WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD or
              WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED or
              WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
      )

      if (isOreoMr1Plus()) {
         setShowWhenLocked(true)
         setTurnScreenOn(true)
         (getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager).requestDismissKeyguard(
            this,
            null
         )
      }
   }
}