package com.river.pink.redux

/**
 * Listener called when the state changes in the [Store].
 */
typealias Observer<S> = (S) -> Unit