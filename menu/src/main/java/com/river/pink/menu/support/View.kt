package com.river.pink.menu.support

import android.view.View
import android.view.ViewTreeObserver
import android.view.inputmethod.InputMethodManager
import androidx.core.content.getSystemService
import androidx.core.view.ViewCompat

/**
 * Is the horizontal layout direction of this view from Right to Left?
 */
val View.isRTL: Boolean
   get() = layoutDirection == ViewCompat.LAYOUT_DIRECTION_RTL

/**
 * Hides the soft input window.
 */
fun View.hideKeyboard() {
   val imm = context.getSystemService<InputMethodManager>()
   imm?.hideSoftInputFromWindow(windowToken, 0)
}

/**
 * Registers a one-time callback to be invoked when the global layout state
 * or the visibility of views within the view tree changes.
 */
inline fun View.onNextGlobalLayout(crossinline callback: () -> Unit) {
   var listener: ViewTreeObserver.OnGlobalLayoutListener? = null
   listener = ViewTreeObserver.OnGlobalLayoutListener {
      viewTreeObserver.removeOnGlobalLayoutListener(listener)
      callback()
   }
   viewTreeObserver.addOnGlobalLayoutListener(listener)
}