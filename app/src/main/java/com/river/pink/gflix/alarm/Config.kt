package com.river.pink.gflix.alarm

import android.content.Context
import android.media.RingtoneManager
import com.river.pink.gflix.gson.gson
import com.simplemobiletools.commons.extensions.getDefaultAlarmSound
import com.simplemobiletools.commons.helpers.BaseConfig

const val DEFAULT_MAX_ALARM_REMINDER_SECS = 300
const val DEFAULT_MAX_TIMER_REMINDER_SECS = 60
const val ALARM_MAX_REMINDER_SECS = "alarm_max_reminder_secs"
const val ALARM_LAST_CONFIG = "alarm_last_config"
const val TIMER_MAX_REMINDER_SECS = "timer_max_reminder_secs"
const val INCREASE_VOLUME_GRADUALLY = "increase_volume_gradually"
const val TIMER_VIBRATE = "timer_vibrate"
const val TIMER_SOUND_URI = "timer_sound_uri"

class Config(context: Context) : BaseConfig(context) {
   companion object {
      fun newInstance(context: Context) = Config(context)
   }

   var alarmLastConfig: Alarm?
      get() = prefs.getString(ALARM_LAST_CONFIG, null)?.let { lastAlarm ->
         gson.fromJson(lastAlarm, Alarm::class.java)
      }
      set(alarm) = prefs.edit().putString(ALARM_LAST_CONFIG, gson.toJson(alarm)).apply()

   var alarmMaxReminderSecs: Int
      get() = prefs.getInt(ALARM_MAX_REMINDER_SECS, DEFAULT_MAX_ALARM_REMINDER_SECS)
      set(alarmMaxReminderSecs) = prefs.edit().putInt(ALARM_MAX_REMINDER_SECS, alarmMaxReminderSecs).apply()

   var timerMaxReminderSecs: Int
      get() = prefs.getInt(TIMER_MAX_REMINDER_SECS, DEFAULT_MAX_TIMER_REMINDER_SECS)
      set(timerMaxReminderSecs) = prefs.edit().putInt(TIMER_MAX_REMINDER_SECS, timerMaxReminderSecs).apply()

   var increaseVolumeGradually: Boolean
      get() = prefs.getBoolean(INCREASE_VOLUME_GRADUALLY, true)
      set(increaseVolumeGradually) = prefs.edit().putBoolean(INCREASE_VOLUME_GRADUALLY, increaseVolumeGradually).apply()

   var timerVibrate: Boolean
      get() = prefs.getBoolean(TIMER_VIBRATE, false)
      set(timerVibrate) = prefs.edit().putBoolean(TIMER_VIBRATE, timerVibrate).apply()

   var timerSoundUri: String
      get() = prefs.getString(TIMER_SOUND_URI, context.getDefaultAlarmSound(RingtoneManager.TYPE_ALARM).uri)!!
      set(timerSoundUri) = prefs.edit().putString(TIMER_SOUND_URI, timerSoundUri).apply()
}