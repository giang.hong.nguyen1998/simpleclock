package com.river.pink.gflix.alarm

import android.app.*
import android.content.Context
import android.content.Intent
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.RingtoneManager
import android.net.Uri
import android.os.PowerManager
import android.text.SpannableString
import android.text.format.DateFormat
import android.text.style.RelativeSizeSpan
import android.widget.Toast
import androidx.core.app.AlarmManagerCompat
import androidx.core.app.NotificationCompat
import com.river.pink.gflix.MainActivity
import com.river.pink.gflix.R
import com.river.pink.gflix.SnoozeReminderActivity
import com.simplemobiletools.commons.extensions.*
import com.simplemobiletools.commons.helpers.DAY_MINUTES
import com.simplemobiletools.commons.helpers.EVERY_DAY_BIT
import com.simplemobiletools.commons.helpers.SILENT
import com.simplemobiletools.commons.helpers.isOreoPlus
import java.util.*
import kotlin.math.pow

const val OPEN_TAB = "open_tab"
const val ALARM_ID = "alarm_id"
const val ALARM_NOTIF_ID = 9998
const val OPEN_ALARMS_TAB_INTENT_ID = 9996
const val TAB_ALARM = 1

const val DEFAULT_ALARM_MINUTES = 480

const val TODAY_BIT = -1
const val TOMORROW_BIT = -2

val Context.config: Config get() = Config.newInstance(applicationContext)
val Context.dbHelper: DBHelper get() = DBHelper.newInstance(applicationContext)

fun Context.checkAlarmsWithDeletedSoundUri(uri: String) {
   val defaultAlarmSound = getDefaultAlarmSound(RingtoneManager.TYPE_ALARM)
   dbHelper.getAlarmsWithUri(uri).forEach {
      it.soundTitle = defaultAlarmSound.title
      it.soundUri = defaultAlarmSound.uri
      dbHelper.updateAlarm(it)
   }
}

fun Context.getSnoozePendingIntent(alarm: Alarm): PendingIntent {
   val snoozeClass = if (config.useSameSnooze) SnoozeJobService::class.java else SnoozeReminderActivity::class.java
   val intent = Intent(this, snoozeClass).setAction("Snooze")
   intent.putExtra(ALARM_ID, alarm.id)
   return if (config.useSameSnooze) {
      PendingIntent.getService(
         this,
         alarm.id,
         intent,
         PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
      )
   } else {
      PendingIntent.getActivity(
         this,
         alarm.id,
         intent,
         PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
      )
   }
}

fun getTomorrowBit(): Int {
   val calendar = Calendar.getInstance()
   calendar.add(Calendar.DAY_OF_WEEK, 1)
   val dayOfWeek = (calendar.get(Calendar.DAY_OF_WEEK) + 5) % 7
   return 2.0.pow(dayOfWeek).toInt()
}

fun Context.rescheduleEnabledAlarms() {
   dbHelper.getEnabledAlarms().forEach {
      if (it.days != TODAY_BIT || it.timeInMinutes > getCurrentDayMinutes()) {
         scheduleNextAlarm(it, false)
      }
   }
}

fun Context.createNewAlarm(timeInMinutes: Int, weekDays: Int): Alarm {
   val defaultAlarmSound = getDefaultAlarmSound(RingtoneManager.TYPE_ALARM)
   return Alarm(
      id = 0,
      timeInMinutes = timeInMinutes,
      days = weekDays,
      isEnabled = false,
      vibrate = false,
      soundTitle = defaultAlarmSound.title,
      soundUri = defaultAlarmSound.uri,
      label = ""
   )
}

fun Context.getAlarmSelectedDaysString(bitMask: Int): String {
   return when (bitMask) {
      TODAY_BIT -> getString(R.string.today)
      TOMORROW_BIT -> getString(R.string.tomorrow)
      EVERY_DAY_BIT -> getString(R.string.every_day)
      else -> getSelectedDaysString(bitMask)
   }
}

fun Context.getNextAlarm(): String {
   val milliseconds =
      (getSystemService(Context.ALARM_SERVICE) as AlarmManager).nextAlarmClock?.triggerTime
         ?: return ""
   val calendar = Calendar.getInstance()
   val isDaylightSavingActive = TimeZone.getDefault().inDaylightTime(Date())
   var offset = calendar.timeZone.rawOffset
   if (isDaylightSavingActive) {
      offset += TimeZone.getDefault().dstSavings
   }

   calendar.timeInMillis = milliseconds
   val dayOfWeekIndex = (calendar.get(Calendar.DAY_OF_WEEK) + 5) % 7
   val dayOfWeek = resources.getStringArray(R.array.week_days_short)[dayOfWeekIndex]
   val formatted = getFormattedTime(
      ((milliseconds + offset) / 1000L).toInt(),
      showSeconds = false,
      makeAmPmSmaller = false
   )
   return "$dayOfWeek $formatted"
}

fun Context.getOpenAlarmTabIntent(): PendingIntent {
   val intent = getLaunchIntent() ?: Intent(this, MainActivity::class.java)
   intent.putExtra(OPEN_TAB, TAB_ALARM)
   return PendingIntent.getActivity(
      this,
      OPEN_ALARMS_TAB_INTENT_ID,
      intent,
      PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
   )
}

fun Context.showAlarmNotification(alarm: Alarm) {
   val pendingIntent = getOpenAlarmTabIntent()
   val notification = getAlarmNotification(pendingIntent, alarm)
   val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
   try {
      notificationManager.notify(alarm.id, notification)
   } catch (e: Exception) {
      showErrorToast(e)
   }

   if (alarm.days > 0) {
      scheduleNextAlarm(alarm, false)
   }
}

fun Context.scheduleNextAlarm(alarm: Alarm, showToast: Boolean) {
   val calendar = Calendar.getInstance()
   calendar.firstDayOfWeek = Calendar.MONDAY
   val currentTimeInMinutes = getCurrentDayMinutes()

   if (alarm.days == TODAY_BIT) {
      val triggerInMinutes = alarm.timeInMinutes - currentTimeInMinutes
      setupAlarmClock(alarm, triggerInMinutes * 60 - calendar.get(Calendar.SECOND))

      if (showToast) {
         showRemainingTimeMessage(triggerInMinutes)
      }
   } else if (alarm.days == TOMORROW_BIT) {
      val triggerInMinutes = alarm.timeInMinutes - currentTimeInMinutes + DAY_MINUTES
      setupAlarmClock(alarm, triggerInMinutes * 60 - calendar.get(Calendar.SECOND))

      if (showToast) {
         showRemainingTimeMessage(triggerInMinutes)
      }
   } else {
      for (i in 0..7) {
         val currentDay = (calendar.get(Calendar.DAY_OF_WEEK) + 5) % 7
         val isCorrectDay = alarm.days and 2.0.pow(currentDay).toInt() != 0
         if (isCorrectDay && (alarm.timeInMinutes > currentTimeInMinutes || i > 0)) {
            val triggerInMinutes = alarm.timeInMinutes - currentTimeInMinutes + (i * DAY_MINUTES)
            setupAlarmClock(alarm, triggerInMinutes * 60 - calendar.get(Calendar.SECOND))

            if (showToast) {
               showRemainingTimeMessage(triggerInMinutes)
            }
            break
         } else {
            calendar.add(Calendar.DAY_OF_MONTH, 1)
         }
      }
   }
}

fun Context.showRemainingTimeMessage(totalMinutes: Int) {
   val fullString =
      String.format(getString(R.string.time_remaining), formatMinutesToTimeString(totalMinutes))
   toast(fullString, Toast.LENGTH_LONG)
}

fun Context.setupAlarmClock(alarm: Alarm, triggerInSeconds: Int) {
   val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
   val targetMS = System.currentTimeMillis() + triggerInSeconds * 1000
   try {
      AlarmManagerCompat.setAlarmClock(
         alarmManager, targetMS, getOpenAlarmTabIntent(), getAlarmIntent(alarm)
      )
   } catch (e: Exception) {
      showErrorToast(e)
   }
}

fun getCurrentDayMinutes(): Int {
   val calendar = Calendar.getInstance()
   return calendar.get(Calendar.HOUR_OF_DAY) * 60 + calendar.get(Calendar.MINUTE)
}

fun Context.getAlarmNotification(pendingIntent: PendingIntent, alarm: Alarm): Notification {
   val soundUri = /*alarm.soundUri*/ "android.resource://" + packageName + "/" + R.raw.default_alarm_alert_homecoming
   if (soundUri != SILENT) {
      grantReadUriPermission(soundUri)
   }

   val channelId = "simple_alarm_channel_$soundUri"
   val label = alarm.label.ifEmpty { getString(R.string.alarm) }

   if (isOreoPlus()) {
      val audioAttributes = AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_ALARM)
         .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
         .setLegacyStreamType(AudioManager.STREAM_ALARM)
         .setFlags(AudioAttributes.FLAG_AUDIBILITY_ENFORCED).build()

      val notificationManager =
         getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
      val importance = NotificationManager.IMPORTANCE_HIGH
      NotificationChannel(channelId, label, importance).apply {
         setBypassDnd(true)
         enableLights(true)
         lightColor = getProperPrimaryColor()
         enableVibration(alarm.vibrate)
         setSound(Uri.parse(soundUri), audioAttributes)
         notificationManager.createNotificationChannel(this)
      }
   }

   val dismissIntent = getHideAlarmPendingIntent(alarm)
   val builder = NotificationCompat.Builder(this).setContentTitle(label)
      .setContentText(
         getFormattedTime(
            getPassedSeconds(),
            showSeconds = false,
            makeAmPmSmaller = false
         )
      )
      .setSmallIcon(R.drawable.ic_alarm_vector)
      .setContentIntent(pendingIntent)
      .setPriority(Notification.PRIORITY_HIGH)
      .setDefaults(Notification.DEFAULT_LIGHTS)
      .setAutoCancel(true)
      .setChannelId(channelId)
      .addAction(R.drawable.ic_snooze_vector, getString(R.string.snooze), getSnoozePendingIntent(alarm))
      .addAction(R.drawable.ic_cross_vector, getString(R.string.dismiss), dismissIntent)
      .setDeleteIntent(dismissIntent)

   builder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC)

   if (soundUri != SILENT) {
      builder.setSound(Uri.parse(soundUri), AudioManager.STREAM_ALARM)
   }

   if (alarm.vibrate) {
      val vibrateArray = LongArray(2) { 500 }
      builder.setVibrate(vibrateArray)
   }

   val notification = builder.build()
   notification.flags = notification.flags or Notification.FLAG_INSISTENT
   return notification
}

fun getPassedSeconds(): Int {
   val calendar = Calendar.getInstance()
   val isDaylightSavingActive = TimeZone.getDefault().inDaylightTime(Date())
   var offset = calendar.timeZone.rawOffset
   if (isDaylightSavingActive) {
      offset += TimeZone.getDefault().dstSavings
   }
   return ((calendar.timeInMillis + offset) / 1000).toInt()
}

fun Context.formatTo12HourFormat(
   showSeconds: Boolean, hours: Int, minutes: Int, seconds: Int
): String {
   val appendable = getString(if (hours >= 12) R.string.p_m else R.string.a_m)
   val newHours = if (hours == 0 || hours == 12) 12 else hours % 12
   return "${formatTime(showSeconds, false, newHours, minutes, seconds)} $appendable"
}

fun Context.getFormattedTime(
   passedSeconds: Int, showSeconds: Boolean, makeAmPmSmaller: Boolean
): SpannableString {
   val use24HourFormat = DateFormat.is24HourFormat(this)
   val hours = (passedSeconds / 3600) % 24
   val minutes = (passedSeconds / 60) % 60
   val seconds = passedSeconds % 60

   return if (use24HourFormat) {
      val formattedTime = formatTime(showSeconds, true, hours, minutes, seconds)
      SpannableString(formattedTime)
   } else {
      val formattedTime = formatTo12HourFormat(showSeconds, hours, minutes, seconds)
      val spannableTime = SpannableString(formattedTime)
      val amPmMultiplier = if (makeAmPmSmaller) 0.4f else 1f
      spannableTime.setSpan(
         RelativeSizeSpan(amPmMultiplier), spannableTime.length - 3, spannableTime.length, 0
      )
      spannableTime
   }
}

fun formatTime(
   showSeconds: Boolean, use24HourFormat: Boolean, hours: Int, minutes: Int, seconds: Int
): String {
   val hoursFormat = if (use24HourFormat) "%02d" else "%01d"
   var format = "$hoursFormat:%02d"

   return if (showSeconds) {
      format += ":%02d"
      String.format(format, hours, minutes, seconds)
   } else {
      String.format(format, hours, minutes)
   }
}

fun Context.hideNotification(id: Int) {
   val manager =
      applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
   manager.cancel(id)
}

fun Context.getHideAlarmPendingIntent(alarm: Alarm): PendingIntent {
   val intent = Intent(this, HideAlarmReceiver::class.java)
   intent.putExtra(ALARM_ID, alarm.id)
   return PendingIntent.getBroadcast(
      this, alarm.id, intent, PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
   )
}

fun Context.getAlarmIntent(alarm: Alarm): PendingIntent {
   val intent = Intent(this, AlarmReceiver::class.java)
   intent.putExtra(ALARM_ID, alarm.id)
   return PendingIntent.getBroadcast(
      this, alarm.id, intent, PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
   )
}

fun Context.isScreenOn() = (getSystemService(Context.POWER_SERVICE) as PowerManager).isScreenOn

fun Context.cancelAlarmClock(alarm: Alarm) {
   val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
   alarmManager.cancel(getAlarmIntent(alarm))
}