package com.river.pink.menu.item

import android.content.Context
import android.view.View
import com.river.pink.menu.R
import com.river.pink.menu.BrowserMenu
import com.river.pink.menu.BrowserMenuItem
import com.river.pink.menu.concept.candidate.ContainerStyle
import com.river.pink.menu.concept.candidate.DividerMenuCandidate

/**
 * A browser menu item to display a horizontal divider.
 */
class BrowserMenuDivider : BrowserMenuItem {
    override var visible: () -> Boolean = { true }

    override val interactiveCount: () -> Int = { 0 }

    override fun getLayoutResource() = R.layout.mozac_browser_menu_item_divider

    override fun bind(menu: BrowserMenu, view: View) = Unit

    override fun asCandidate(context: Context) = DividerMenuCandidate(
        containerStyle = ContainerStyle(isVisible = visible())
    )
}
