package com.river.pink.redux

/**
 * Generic interface for actions to be dispatched on a [Store].
 *
 * Actions are used to send data from the application to a [Store]. The [Store] will use the [Action] to
 * derive a new [State]. Actions should describe what happened, while [Reducer]s will describe how the
 * state changes.
 */
interface Action