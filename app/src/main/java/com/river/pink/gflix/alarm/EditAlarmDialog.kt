package com.river.pink.gflix.alarm

import android.annotation.SuppressLint
import android.app.TimePickerDialog
import android.content.Intent
import android.graphics.drawable.Drawable
import android.media.AudioManager
import android.media.RingtoneManager
import android.text.format.DateFormat
import android.widget.TextView
import androidx.activity.result.ActivityResultLauncher
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import com.river.pink.gflix.R
import com.river.pink.gflix.databinding.DialogEditAlarmBinding
import com.simplemobiletools.commons.dialogs.ConfirmationDialog
import com.simplemobiletools.commons.extensions.*
import com.simplemobiletools.commons.models.AlarmSound
import kotlin.math.pow

class EditAlarmDialog(
   private val activity: BaseSimpleActivity,
   private val resultLauncher: ActivityResultLauncher<Intent>,
   val alarm: Alarm,
   val callback: (alarmId: Int) -> Unit
) {
   @SuppressLint("InflateParams")
   private val view = activity.layoutInflater.inflate(R.layout.dialog_edit_alarm, null)
   private val binding get() = DialogEditAlarmBinding.bind(view)
   private val textColor = activity.getProperTextColor()

   init {
      restoreLastAlarm()
      updateAlarmTime()

      binding.apply {
         editAlarmTime.setOnClickListener {
            TimePickerDialog(
               activity,
               activity.getTimePickerDialogTheme(),
               timeSetListener,
               alarm.timeInMinutes / 60,
               alarm.timeInMinutes % 60,
               DateFormat.is24HourFormat(activity)
            ).show()
         }

         editAlarmSound.colorCompoundDrawable(textColor)
         editAlarmSound.text = alarm.soundTitle
         editAlarmSound.setOnClickListener {
            SelectAlarmSoundDialog(
               activity,
               resultLauncher,
               alarm.soundUri,
               AudioManager.STREAM_ALARM,
               RingtoneManager.TYPE_ALARM,
               true,
               onAlarmPicked = {
                  if (it != null) {
                     updateSelectedAlarmSound(it)
                  }
               },
               onAlarmSoundDeleted = {
                  if (alarm.soundUri == it.uri) {
                     val defaultAlarm = activity.getDefaultAlarmSound(RingtoneManager.TYPE_ALARM)
                     updateSelectedAlarmSound(defaultAlarm)
                  }
                  activity.checkAlarmsWithDeletedSoundUri(it.uri)
               })
         }

         editAlarmVibrateIcon.setColorFilter(textColor)
         editAlarmVibrate.isChecked = alarm.vibrate
         editAlarmVibrateHolder.setOnClickListener {
            editAlarmVibrate.toggle()
            alarm.vibrate = editAlarmVibrate.isChecked
         }

         editAlarmLabelImage.applyColorFilter(textColor)
         editAlarm.setText(alarm.label)

         val dayLetters = activity.resources.getStringArray(R.array.week_day_letters)
            .toList() as ArrayList<String>
         val dayIndexes = arrayListOf(0, 1, 2, 3, 4, 5, 6)
         if (activity.config.isSundayFirst) {
            dayIndexes.moveLastItemToFront()
         }

         dayIndexes.forEach {
            val pow = 2.0.pow(it.toDouble()).toInt()
            val day = activity.layoutInflater.inflate(
               R.layout.alarm_day,
               editAlarmDaysHolder,
               false
            ) as TextView
            day.text = dayLetters[it]

            val isDayChecked = alarm.days > 0 && alarm.days and pow != 0
            day.background = getProperDayDrawable(isDayChecked)

            day.setTextColor(if (isDayChecked) activity.getProperBackgroundColor() else textColor)
            day.setOnClickListener {
               if (alarm.days < 0) {
                  alarm.days = 0
               }

               val selectDay = alarm.days and pow == 0
               if (selectDay) {
                  alarm.days = alarm.days.addBit(pow)
               } else {
                  alarm.days = alarm.days.removeBit(pow)
               }
               day.background = getProperDayDrawable(selectDay)
               day.setTextColor(if (selectDay) activity.getProperBackgroundColor() else textColor)
               checkDaylessAlarm()
            }

            editAlarmDaysHolder.addView(day)
         }
      }

      activity.getAlertDialogBuilder()
         .setPositiveButton(R.string.ok, null)
         .setNegativeButton(R.string.cancel, null)
         .apply {
            activity.setupDialogStuff(view, this) { alertDialog ->
               alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
                  if (!activity.config.wasAlarmWarningShown) {
                     ConfirmationDialog(
                        activity,
                        messageId = R.string.alarm_warning,
                        positive = R.string.ok,
                        negative = 0
                     ) {
                        activity.config.wasAlarmWarningShown = true
                        it.performClick()
                     }

                     return@setOnClickListener
                  }

                  if (alarm.days <= 0) {
                     alarm.days = if (alarm.timeInMinutes > getCurrentDayMinutes()) {
                        TODAY_BIT
                     } else {
                        TOMORROW_BIT
                     }
                  }

                  alarm.label = binding.editAlarm.value
                  alarm.isEnabled = true

                  var alarmId = alarm.id
                  activity.handleNotificationPermission {
                     if (it) {
                        if (alarm.id == 0) {
                           alarmId = activity.dbHelper.insertAlarm(alarm)
                           if (alarmId == -1) {
                              activity.toast(R.string.unknown_error_occurred)
                           }
                        } else {
                           if (!activity.dbHelper.updateAlarm(alarm)) {
                              activity.toast(R.string.unknown_error_occurred)
                           }
                        }

                        activity.config.alarmLastConfig = alarm
                        callback(alarmId)
                        alertDialog.dismiss()
                     } else {
                        activity.toast(R.string.no_post_notifications_permissions)
                     }
                  }
               }
            }
         }
   }

   private fun restoreLastAlarm() {
      if (alarm.id == 0) {
         activity.config.alarmLastConfig?.let { lastConfig ->
            alarm.label = lastConfig.label
            alarm.days = lastConfig.days
            alarm.soundTitle = lastConfig.soundTitle
            alarm.soundUri = lastConfig.soundUri
            alarm.timeInMinutes = lastConfig.timeInMinutes
            alarm.vibrate = lastConfig.vibrate
         }
      }
   }

   private val timeSetListener = TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->
      alarm.timeInMinutes = hourOfDay * 60 + minute
      updateAlarmTime()
   }

   private fun updateAlarmTime() {
      binding.editAlarmTime.text = activity.getFormattedTime(
         alarm.timeInMinutes * 60,
         showSeconds = false,
         makeAmPmSmaller = true
      )
      checkDaylessAlarm()
   }

   @SuppressLint("SetTextI18n")
   private fun checkDaylessAlarm() {
      if (alarm.days <= 0) {
         val textId = if (alarm.timeInMinutes > getCurrentDayMinutes()) {
            R.string.today
         } else {
            R.string.tomorrow
         }

         binding.editAlarmDaylessLabel.text = "(${activity.getString(textId)})"
      }
      binding.editAlarmDaylessLabel.beVisibleIf(alarm.days <= 0)
   }

   private fun getProperDayDrawable(selected: Boolean): Drawable {
      val drawableId =
         if (selected) R.drawable.circle_background_filled else R.drawable.circle_background_stroke
      val drawable = ContextCompat.getDrawable(activity, drawableId)
      drawable!!.applyColorFilter(textColor)
      return drawable
   }

   private fun updateSelectedAlarmSound(alarmSound: AlarmSound) {
      alarm.soundTitle = alarmSound.title
      alarm.soundUri = alarmSound.uri
      binding.editAlarmSound.text = alarmSound.title
   }
}