package com.river.pink.gflix

import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.river.pink.menu.support.resolveAttribute
import com.river.pink.menu.view.MenuButton
import java.lang.ref.WeakReference

class TestActivity : AppCompatActivity(R.layout.activity_main) {

   private var menuButton: MenuButton? = null
   private lateinit var store: MainStore

   override fun onCreate(savedInstanceState: Bundle?) {
      super.onCreate(savedInstanceState)

      store = StoreProvider.get(this) {
         MainStore(initialState = MainState(), middlewares = listOf())
      }

      menuButton?.setColorFilter(
         ContextCompat.getColor(
            this,
            resolveAttribute(R.attr.primaryText)
         )
      )

      createHomeMenu(this, WeakReference(menuButton))
   }

   override fun onConfigurationChanged(newConfig: Configuration) {
      super.onConfigurationChanged(newConfig)

      menuButton?.dismissMenu()
   }

   private fun createHomeMenu(context: Context, menuButtonView: WeakReference<MenuButton>) =
      HomeMenu(
         context,
         onItemTapped = {
            when (it) {
               HomeMenu.Item.Settings -> {

               }
               HomeMenu.Item.CustomizeHome -> {
               }
               HomeMenu.Item.Bookmarks -> {
               }
               HomeMenu.Item.History -> {
               }

               HomeMenu.Item.Downloads -> {
               }

               HomeMenu.Item.Help -> {
               }
               HomeMenu.Item.WhatsNew -> {
               }
               // We need to show the snackbar while the browsing data is deleting(if "Delete
               // browsing data on quit" is activated). After the deletion is over, the snackbar
               // is dismissed.
               HomeMenu.Item.Quit -> {
               }
               HomeMenu.Item.ReconnectSync -> {
               }
               HomeMenu.Item.Extensions -> {
               }
               is HomeMenu.Item.DesktopMode -> {
               }
            }
         },
         onHighlightPresent = { menuButtonView.get()?.setHighlight(it) },
         onMenuBuilderChanged = { menuButtonView.get()?.menuBuilder = it }
      )
}