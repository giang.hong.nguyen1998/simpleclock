package com.river.pink.gflix

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.res.Resources
import android.os.Build
import android.view.View
import android.view.ViewPropertyAnimator
import androidx.core.view.animation.PathInterpolatorCompat
import kotlin.math.abs
import kotlin.math.roundToInt

interface TranslateAnimationListener {
   fun doOnStart() {}
   fun doOnEnd() {}
}

class TranslateAnimator(val view: View, val height: Float) {

   var viewAnimator: ViewPropertyAnimator? = null
      private set

   var animator: Animator? = null
      private set

   fun cancelAnimator() {
      viewAnimator?.cancel()
      animator?.cancel()
   }

   fun startAnimator(
      isShowing: Boolean,
      swapHeight: Boolean,
      transitionHeight: Float = height,
      isAlpha: Boolean,
      listener: TranslateAnimationListener? = null
   ) {
      var height = transitionHeight
      var visibility = 0
      var alpha = 1f
      val isVisibleAlpha = view.visibility == View.VISIBLE && isNaN(view.alpha, 1f)
      val isVisible = view.visibility != View.VISIBLE || isNaN(view.alpha, 0f)
      when {
         isShowing && isVisibleAlpha && isNaN(view.translationY, 0f) || !isShowing && isVisible -> {
            animator ?: return
            animator?.cancel()
         }
         IS_ROBOLECTRIC -> {
            if (!isShowing) visibility = View.GONE
            view.visibility = visibility
            view.alpha = 1.0f
            listener?.doOnEnd()
         }
         else -> {
            if (height <= 0) height = this.height
            if (isShowing) {
               view.visibility = View.VISIBLE
               if (view.alpha == 1.0f) view.alpha = 0.0f
               if (view.translationY == 0.0f) view.translationY =
                  if (swapHeight) height else -height
            }
            height = if (isShowing) 0f else if (!swapHeight) -height else height
            val ofFloat = ObjectAnimator.ofFloat(view, View.TRANSLATION_Y, height).apply {
               duration = 300L
               interpolator = TRANSLATE_INTERPOLATOR
            }
            if (isShowing) alpha = 0f
            val duration = ObjectAnimator.ofFloat(
               view,
               View.ALPHA,
               alpha,
               if (!isShowing || isAlpha) if (!isShowing) 0f else 0.3f else 1f
            ).setDuration(150L)
            if (isShowing && view.alpha == 0f) visibility = 100
            val animatorSet = AnimatorSet()
            animatorSet.startDelay = visibility.toLong()
            animatorSet.play(ofFloat).with(duration)
            animatorSet.addListener(object : AnimatorListenerAdapter() {
               private var isAnimationCancel = false

               override fun onAnimationStart(animation: Animator) {
                  listener?.doOnStart()
               }

               override fun onAnimationCancel(animator: Animator) {
                  isAnimationCancel = true
               }

               override fun onAnimationEnd(animator: Animator) {
                  if (!isAnimationCancel) {
                     view.visibility = if (isShowing) View.VISIBLE else View.GONE
                  }
                  listener?.doOnEnd()
               }
            })
            animator?.cancel()
            animator = animatorSet
            animator?.start()
         }
      }
   }

   private fun isNaN(a: Float, b: Float): Boolean {
      return if (a.isNaN() || b.isNaN()) a.isNaN() && b.isNaN() else abs(b - a) < 1.0E-5f
   }

   companion object {
      private val IS_ROBOLECTRIC = "robolectric" == Build.FINGERPRINT
      private val TRANSLATE_INTERPOLATOR = PathInterpolatorCompat.create(0.23f, 1.0f, 0.32f, 1.0f)
   }
}

val Int.dp: Int
   get() = (this * Resources.getSystem().displayMetrics.density).roundToInt()

val Float.dp: Int
   get() = (this * Resources.getSystem().displayMetrics.density).roundToInt()