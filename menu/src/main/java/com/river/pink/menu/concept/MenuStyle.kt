package com.river.pink.menu.concept

import android.content.res.ColorStateList
import androidx.annotation.ColorInt
import androidx.annotation.Px

/**
 * Declare custom styles for a menu.
 *
 * @property backgroundColor Custom background color for the menu.
 * @property minWidth Custom minimum width for the menu.
 * @property maxWidth Custom maximum width for the menu.
 */
data class MenuStyle(
    val backgroundColor: ColorStateList? = null,
    @Px val minWidth: Int? = null,
    @Px val maxWidth: Int? = null
) {
    constructor(
        @ColorInt backgroundColor: Int,
        @Px minWidth: Int? = null,
        @Px maxWidth: Int? = null
    ) : this(
        backgroundColor = ColorStateList.valueOf(backgroundColor),
        minWidth = minWidth,
        maxWidth = maxWidth
    )
}
