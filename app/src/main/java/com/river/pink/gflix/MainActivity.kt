package com.river.pink.gflix

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import com.river.pink.gflix.alarm.*
import com.simplemobiletools.commons.extensions.storeNewYourAlarmSound
import com.simplemobiletools.commons.extensions.toast
import com.simplemobiletools.commons.helpers.ensureBackgroundThread
import com.simplemobiletools.commons.views.MyFloatingActionButton
import com.simplemobiletools.commons.views.MyRecyclerView

class MainActivity : BaseSimpleActivity(), ToggleAlarmInterface {

   private var currentEditAlarmDialog: EditAlarmDialog? = null
   private var alarms = ArrayList<Alarm>()
   private lateinit var resultLauncher: ActivityResultLauncher<Intent>

   override fun onCreate(savedInstanceState: Bundle?) {
      super.onCreate(savedInstanceState)
      setContentView(R.layout.activity_main)

      findViewById<MyFloatingActionButton>(R.id.alarm_fab).setOnClickListener {
         val newAlarm = createNewAlarm(DEFAULT_ALARM_MINUTES, 0)
         newAlarm.isEnabled = true
         newAlarm.days = getTomorrowBit()
         openEditAlarm(newAlarm)
      }

      resultLauncher = registerForActivityResult(
         ActivityResultContracts.StartActivityForResult()
      ) { result ->
         if (result.resultCode == Activity.RESULT_OK) {
            result.data?.let { storeNewYourAlarmSound(it) }
         }
      }

      setupAlarms()
   }

   private fun setupAlarms() {
      alarms = dbHelper.getAlarms()
      alarms.sortBy { it.id }

      if (getNextAlarm().isEmpty()) {
         alarms.forEach {
            if (it.days == TODAY_BIT && it.isEnabled && it.timeInMinutes <= getCurrentDayMinutes()) {
               it.isEnabled = false
               ensureBackgroundThread {
                  dbHelper.updateAlarmEnabledState(it.id, false)
               }
            }
         }
      }

      val alarmList = findViewById<MyRecyclerView>(R.id.alarms_list)

      val currAdapter = alarmList.adapter
      if (currAdapter == null) {
         AlarmsAdapter(this, alarms, this, alarmList) {
            openEditAlarm(it as Alarm)
         }.apply {
            alarmList.adapter = this
         }
      } else {
         (currAdapter as AlarmsAdapter).updateItems(alarms)
      }
   }

   private fun openEditAlarm(alarm: Alarm) {
      currentEditAlarmDialog = EditAlarmDialog(this, resultLauncher, alarm) {
         alarm.id = it
         currentEditAlarmDialog = null
         setupAlarms()
         checkAlarmState(alarm)
      }
   }

   override fun alarmToggled(id: Int, isEnabled: Boolean) {
      handleNotificationPermission { granted ->
         if (granted) {
            if (dbHelper.updateAlarmEnabledState(id, isEnabled)) {
               val alarm = alarms.firstOrNull { it.id == id } ?: return@handleNotificationPermission
               alarm.isEnabled = isEnabled
               checkAlarmState(alarm)
            } else {
               toast(R.string.unknown_error_occurred)
            }
         } else {
            toast(R.string.no_post_notifications_permissions)
         }
      }
   }

   private fun checkAlarmState(alarm: Alarm) {
      if (alarm.isEnabled) {
         scheduleNextAlarm(alarm, true)
      } else {
         cancelAlarmClock(alarm)
      }
   }
}