package com.river.pink.menu.concept.candidate

/**
 * Small icon button menu option. Can only be used with [RowMenuCandidate].
 *
 * @property contentDescription Description of the icon.
 * @property icon Icon to display.
 * @property containerStyle Styling to apply to the container.
 * @property onLongClick Listener called when this menu option is long clicked.
 * @property onClick Click listener called when this menu option is clicked.
 */
data class SmallMenuCandidate(
   val contentDescription: String,
   val icon: DrawableMenuIcon,
   val containerStyle: ContainerStyle = ContainerStyle(),
   val onLongClick: (() -> Boolean)? = null,
   val onClick: () -> Unit = {}
)
