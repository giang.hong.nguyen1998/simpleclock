package com.river.pink.gflix

import com.river.pink.redux.Action
import com.river.pink.redux.Middleware
import com.river.pink.redux.State
import com.river.pink.redux.Store

class MainStore(
   initialState: MainState,
   middlewares: List<Middleware<MainState, MainAction>>
) : Store<MainState, MainAction>(
   initialState, ::mainStateReducer, middlewares
)

data class MainState(
   val showMenu: Boolean = false,
   val error: String? = null
) : State

sealed class MainAction : Action {
   data class ToggleMenu(val showMenu: Boolean) : MainAction()
}

private fun mainStateReducer(
   state: MainState,
   action: MainAction
): MainState {
   return when (action) {
      is MainAction.ToggleMenu -> state.copy(showMenu = action.showMenu)
   }
}