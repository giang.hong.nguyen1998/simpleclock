package com.river.pink.gflix.alarm

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class BootCompletedReceiver : BroadcastReceiver() {

   @SuppressLint("UnsafeProtectedBroadcastReceiver")
   override fun onReceive(context: Context, intent: Intent) {
      context.rescheduleEnabledAlarms()
   }
}