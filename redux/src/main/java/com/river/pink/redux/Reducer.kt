package com.river.pink.redux

/**
 * Reducers specify how the application's [State] changes in response to [Action]s sent to the [Store].
 *
 * Remember that actions only describe what happened, but don't describe how the application's state changes.
 * Reducers will commonly consist of a `when` statement returning different copies of the [State].
 */
typealias Reducer<S, A> = (S, A) -> S