package com.river.pink.menu.concept.candidate

/**
 * Describes styling for the menu option container.
 *
 * @property isVisible When false, the option will not be displayed.
 * @property isEnabled When false, the option will be greyed out and disabled.
 */
data class ContainerStyle(
    val isVisible: Boolean = true,
    val isEnabled: Boolean = true
)
