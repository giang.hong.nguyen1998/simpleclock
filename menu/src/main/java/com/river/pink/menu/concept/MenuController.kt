package com.river.pink.menu.concept

import android.view.View
import android.widget.PopupWindow
import com.river.pink.menu.concept.candidate.MenuCandidate
import com.river.pink.menu.support.Observable

/**
 * Controls a popup menu composed of MenuCandidate objects.
 */
interface MenuController : Observable<MenuController.Observer> {

    /**
     * @param anchor The view on which to pin the popup window.
     * @param orientation The preferred orientation to show the popup window.
     */
    fun show(anchor: View, orientation: Orientation? = null): PopupWindow

    /**
     * Dismiss the menu popup if the menu is visible.
     */
    fun dismiss()

    /**
     * Changes the contents of the menu.
     */
    fun submitList(list: List<MenuCandidate>)

    /**
     * Observer for the menu controller.
     */
    interface Observer {
        /**
         * Called when the menu contents have changed.
         */
        fun onMenuListSubmit(list: List<MenuCandidate>) = Unit

        /**
         * Called when the menu has been dismissed.
         */
        fun onDismiss() = Unit
    }
}
