package com.river.pink.gflix.alarm

import android.content.Context
import android.graphics.Color
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.simplemobiletools.commons.extensions.*
import com.simplemobiletools.commons.helpers.MyContextWrapper
import com.simplemobiletools.commons.helpers.PERMISSION_POST_NOTIFICATIONS
import com.simplemobiletools.commons.helpers.isTiramisuPlus

abstract class BaseSimpleActivity : AppCompatActivity() {
   private var actionOnPermission: ((granted: Boolean) -> Unit)? = null
   private var isAskingPermissions = false

   companion object {
      private const val GENERIC_PERM_HANDLER = 100
   }

   override fun onDestroy() {
      super.onDestroy()
      actionOnPermission = null
   }

   override fun onOptionsItemSelected(item: MenuItem): Boolean {
      when (item.itemId) {
         android.R.id.home -> {
            hideKeyboard()
            finish()
         }
         else -> return super.onOptionsItemSelected(item)
      }
      return true
   }

   override fun attachBaseContext(newBase: Context) {
      if (newBase.baseConfig.useEnglish && !isTiramisuPlus()) {
         super.attachBaseContext(MyContextWrapper(newBase).wrap(newBase, "en"))
      } else {
         super.attachBaseContext(newBase)
      }
   }

   fun updateMenuItemColors(
      menu: Menu?,
      baseColor: Int = getProperStatusBarColor(),
      forceWhiteIcons: Boolean = false
   ) {
      if (menu == null) {
         return
      }

      var color = baseColor.getContrastColor()
      if (forceWhiteIcons) {
         color = Color.WHITE
      }

      for (i in 0 until menu.size()) {
         try {
            menu.getItem(i)?.icon?.setTint(color)
         } catch (ignored: Exception) {
         }
      }
   }

   fun handlePermission(permissionId: Int, callback: (granted: Boolean) -> Unit) {
      actionOnPermission = null
      if (hasPermission(permissionId)) {
         callback(true)
      } else {
         isAskingPermissions = true
         actionOnPermission = callback
         ActivityCompat.requestPermissions(
            this,
            arrayOf(getPermissionString(permissionId)),
            GENERIC_PERM_HANDLER
         )
      }
   }

   fun handleNotificationPermission(callback: (granted: Boolean) -> Unit) {
      if (!isTiramisuPlus()) {
         callback(true)
      } else {
         handlePermission(PERMISSION_POST_NOTIFICATIONS) { granted ->
            callback(granted)
         }
      }
   }

   override fun onRequestPermissionsResult(
      requestCode: Int,
      permissions: Array<String>,
      grantResults: IntArray
   ) {
      super.onRequestPermissionsResult(requestCode, permissions, grantResults)
      isAskingPermissions = false
      if (requestCode == GENERIC_PERM_HANDLER && grantResults.isNotEmpty()) {
         actionOnPermission?.invoke(grantResults[0] == 0)
      }
   }
}
