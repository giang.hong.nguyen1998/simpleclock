package com.river.pink.gflix.alarm

import android.annotation.SuppressLint
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import com.river.pink.gflix.MainActivity
import com.river.pink.gflix.R
import com.river.pink.gflix.databinding.ItemAlarmBinding
import com.simplemobiletools.commons.dialogs.ConfirmationDialog
import com.simplemobiletools.commons.extensions.beVisibleIf
import com.simplemobiletools.commons.extensions.isVisible
import com.simplemobiletools.commons.extensions.toast
import com.simplemobiletools.commons.views.MyRecyclerView

class AlarmsAdapter(
   activity: MainActivity,
   private var alarms: ArrayList<Alarm>,
   private val toggleAlarmInterface: ToggleAlarmInterface,
   recyclerView: MyRecyclerView, itemClick: (Any) -> Unit
) : RemoteRecyclerViewAdapter(activity, recyclerView, itemClick) {

   init {
      setupDragListener(true)
   }

   override fun getActionMenuId() = R.menu.cab_alarms

   override fun prepareActionMode(menu: Menu) {}

   override fun actionItemPressed(id: Int) {
      if (selectedKeys.isEmpty()) {
         return
      }

      when (id) {
         R.id.cab_delete -> deleteItems()
      }
   }

   override fun getSelectableItemCount() = alarms.size

   override fun getIsItemSelectable(position: Int) = true

   override fun getItemSelectionKey(position: Int) = alarms.getOrNull(position)?.id

   override fun getItemKeyPosition(key: Int) = alarms.indexOfFirst { it.id == key }

   override fun onActionModeCreated() {}

   override fun onActionModeDestroyed() {}

   override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
      createViewHolder(R.layout.item_alarm, parent)

   override fun onBindViewHolder(holder: ViewHolder, position: Int) {
      val alarm = alarms[position]
      holder.bindView(
         alarm,
         allowSingleClick = true,
         allowLongClick = true
      ) { itemView, _ ->
         setupView(itemView, alarm)
      }
      bindViewHolder(holder)
   }

   override fun getItemCount() = alarms.size

   @SuppressLint("NotifyDataSetChanged")
   fun updateItems(newItems: ArrayList<Alarm>) {
      alarms = newItems
      notifyDataSetChanged()
      finishActMode()
   }

   private fun deleteItems() {
      val alarmsToRemove = ArrayList<Alarm>()
      val positions = getSelectedItemPositions()
      getSelectedItems().forEach {
         alarmsToRemove.add(it)
      }

      alarms.removeAll(alarmsToRemove.toSet())
      removeSelectedItems(positions)
      activity.dbHelper.deleteAlarms(alarmsToRemove)
   }

   private fun getSelectedItems() =
      alarms.filter { selectedKeys.contains(it.id) } as ArrayList<Alarm>

   private fun setupView(view: View, alarm: Alarm) {
      val isSelected = selectedKeys.contains(alarm.id)
      val binding = ItemAlarmBinding.bind(view)
      binding.apply {
         alarmFrame.isSelected = isSelected
         alarmTime.text = activity.getFormattedTime(
            alarm.timeInMinutes * 60,
            showSeconds = false,
            makeAmPmSmaller = true
         )
         alarmTime.setTextColor(textColor)

         alarmDays.text = activity.getAlarmSelectedDaysString(alarm.days)
         alarmDays.setTextColor(textColor)

         alarmLabel.text = alarm.label
         alarmLabel.setTextColor(textColor)
         alarmLabel.beVisibleIf(alarm.label.isNotEmpty())

         alarmSwitch.isChecked = alarm.isEnabled
         alarmSwitch.setColors(textColor, properPrimaryColor, backgroundColor)
         alarmSwitch.setOnClickListener {
            if (alarm.days > 0) {
               if (activity.config.wasAlarmWarningShown) {
                  toggleAlarmInterface.alarmToggled(alarm.id, alarmSwitch.isChecked)
               } else {
                  ConfirmationDialog(
                     activity,
                     messageId = R.string.alarm_warning,
                     positive = R.string.ok,
                     negative = 0
                  ) {
                     activity.config.wasAlarmWarningShown = true
                     toggleAlarmInterface.alarmToggled(alarm.id, alarmSwitch.isChecked)
                  }
               }
            } else if (alarm.days == TODAY_BIT) {
               if (alarm.timeInMinutes <= getCurrentDayMinutes()) {
                  alarm.days = TOMORROW_BIT
                  alarmDays.text = resources.getString(R.string.tomorrow)
               }
               activity.dbHelper.updateAlarm(alarm)
               activity.scheduleNextAlarm(alarm, true)
               toggleAlarmInterface.alarmToggled(alarm.id, alarmSwitch.isChecked)
            } else if (alarm.days == TOMORROW_BIT) {
               toggleAlarmInterface.alarmToggled(alarm.id, alarmSwitch.isChecked)
            } else if (alarmSwitch.isChecked) {
               activity.toast(R.string.no_days_selected)
               alarmSwitch.isChecked = false
            } else {
               toggleAlarmInterface.alarmToggled(alarm.id, alarmSwitch.isChecked)
            }
         }

         val layoutParams = alarmSwitch.layoutParams as RelativeLayout.LayoutParams
         layoutParams.addRule(
            RelativeLayout.ALIGN_BOTTOM,
            if (alarmLabel.isVisible()) alarmLabel.id else alarmDays.id
         )
      }
   }
}