package com.river.pink.menu.ext

import android.content.Context
import com.river.pink.menu.BrowserMenuHighlight
import com.river.pink.menu.BrowserMenuItem
import com.river.pink.menu.HighlightableMenuItem

/**
 * Get the highlight effect present in the list of menu items, if any.
 */
@Suppress("Deprecation")
fun List<BrowserMenuItem>.getHighlight() = asSequence()
    .filter { it.visible() }
    .mapNotNull { it as? HighlightableMenuItem }
    .filter { it.isHighlighted() }
    .map { it.highlight }
    .filter { it.canPropagate }
    .maxByOrNull {
        // Select the highlight with the highest priority
        when (it) {
            is BrowserMenuHighlight.HighPriority -> 2
            is BrowserMenuHighlight.LowPriority -> 1
            is BrowserMenuHighlight.ClassicHighlight -> 0
        }
    }

/**
 * Converts the menu items into a menu candidate list.
 */
fun List<BrowserMenuItem>.asCandidateList(context: Context) =
    mapNotNull { it.asCandidate(context) }
