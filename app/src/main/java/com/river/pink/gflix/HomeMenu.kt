package com.river.pink.gflix

import android.content.Context
import androidx.core.content.ContextCompat.getColor
import com.river.pink.menu.BrowserMenuBuilder
import com.river.pink.menu.BrowserMenuHighlight
import com.river.pink.menu.BrowserMenuItem
import com.river.pink.menu.ext.getHighlight
import com.river.pink.menu.item.*
import com.river.pink.menu.support.resolveAttribute

class HomeMenu(
   private val context: Context,
   private val onItemTapped: (Item) -> Unit = {},
   private val onMenuBuilderChanged: (BrowserMenuBuilder) -> Unit = {},
   private val onHighlightPresent: (BrowserMenuHighlight) -> Unit = {}
) {
   sealed class Item {
      object Bookmarks : Item()
      object History : Item()
      object Downloads : Item()
      object Extensions : Item()
      object WhatsNew : Item()
      object Help : Item()
      object CustomizeHome : Item()
      object Settings : Item()
      object Quit : Item()
      object ReconnectSync : Item()
      data class DesktopMode(val checked: Boolean) : Item()
   }

   private val primaryTextColor = context.resolveAttribute(R.attr.primaryText)
   private val syncDisconnectedColor = context.resolveAttribute(R.attr.syncDisconnected)
   private val syncDisconnectedBackgroundColor = context.resolveAttribute(R.attr.syncDisconnectedBackground)

   // 'Reconnect' and 'Quit' items aren't needed most of the time, so we'll only create the if necessary.
   private val reconnectToSyncItem by lazy {
      BrowserMenuHighlightableItem(
         context.getString(R.string.sync_reconnect),
         R.drawable.ic_sync_disconnected,
         iconTintColorResource = syncDisconnectedColor,
         textColorResource = primaryTextColor,
         highlight = BrowserMenuHighlight.HighPriority(
            backgroundTint = syncDisconnectedBackgroundColor,
            canPropagate = false
         ),
         isHighlighted = { false }
      ) {
         onItemTapped.invoke(Item.ReconnectSync)
      }
   }

   private val toolbarItem by lazy {
      val backButton = BrowserMenuItemToolbar.Button(
         R.drawable.ic_baseline_arrow_back_24,
         contentDescription = "Arrow Back",
         iconTintColorResource = primaryTextColor,
         listener = {}
      )

      val forwardButton = BrowserMenuItemToolbar.Button(
         R.drawable.ic_baseline_arrow_forward_24,
         contentDescription = "Arrow Forward",
         iconTintColorResource = primaryTextColor,
         listener = {}
      )

      val shareButton = BrowserMenuItemToolbar.Button(
         R.drawable.ic_baseline_share_24,
         contentDescription = "Share",
         iconTintColorResource = primaryTextColor,
         listener = {}
      )

      val refreshButton = BrowserMenuItemToolbar.Button(
         R.drawable.ic_baseline_refresh_24,
         contentDescription = "Refresh",
         iconTintColorResource = primaryTextColor,
         listener = {}
      )

      BrowserMenuItemToolbar(
         listOf(backButton, forwardButton, shareButton, refreshButton),
         isSticky = true,
      )
   }

   private val quitItem by lazy {
      BrowserMenuImageText(
         context.getString(R.string.delete_browsing_data_on_quit_action),
         R.drawable.ic_exit,
         primaryTextColor,
      ) {
         onItemTapped.invoke(Item.Quit)
      }
   }

   private val syncSignInMenuItem = BrowserMenuImageText(
      context.getString(R.string.sync_reconnect),
      R.drawable.ic_synced_tabs,
      primaryTextColor,
   ) {
   }

   val desktopItem = BrowserMenuImageSwitch(
      imageResource = R.drawable.ic_desktop,
      label = context.getString(R.string.browser_menu_desktop_site),
      initialState = { false },
   ) { checked ->
      onItemTapped.invoke(Item.DesktopMode(checked))
   }

   @Suppress("ComplexMethod")
   private fun coreMenuItems(): List<BrowserMenuItem> {
      val bookmarksItem = BrowserMenuImageText(
         context.getString(R.string.library_bookmarks),
         R.drawable.ic_bookmark_list,
         primaryTextColor,
      ) {
         onItemTapped.invoke(Item.Bookmarks)
      }

      val historyItem = BrowserMenuImageText(
         context.getString(R.string.library_history),
         R.drawable.ic_history,
         primaryTextColor
      ) {
         onItemTapped.invoke(Item.History)
      }

      val downloadsItem = BrowserMenuImageText(
         context.getString(R.string.library_downloads),
         R.drawable.ic_download,
         primaryTextColor
      ) {
         onItemTapped.invoke(Item.Downloads)
      }

      val extensionsItem = BrowserMenuImageText(
         context.getString(R.string.browser_menu_add_ons),
         R.drawable.ic_addons_extensions,
         primaryTextColor
      ) {
         onItemTapped.invoke(Item.Extensions)
      }

      val whatsNewItem = BrowserMenuHighlightableItem(
         context.getString(R.string.browser_menu_whats_new),
         R.drawable.ic_whats_new,
         iconTintColorResource = primaryTextColor,
         highlight = BrowserMenuHighlight.LowPriority(
            notificationTint = getColor(context, R.color.whats_new_notification_color)
         ),
         isHighlighted = { true }
      ) {
         onItemTapped.invoke(Item.WhatsNew)
      }

      val helpItem = BrowserMenuImageText(
         context.getString(R.string.browser_menu_help),
         R.drawable.mozac_ic_help,
         primaryTextColor
      ) {
         onItemTapped.invoke(Item.Help)
      }

      val customizeHomeItem = BrowserMenuImageText(
         context.getString(R.string.browser_menu_customize_home),
         R.drawable.ic_customize,
         primaryTextColor
      ) {
         onItemTapped.invoke(Item.CustomizeHome)
      }

      // Use nimbus to set the icon and title.
      val settingsItem = BrowserMenuImageText(
         context.getString(R.string.browser_menu_settings),
         R.drawable.mozac_ic_settings,
         primaryTextColor
      ) {
         onItemTapped.invoke(Item.Settings)
      }

      // Only query account manager if it has been initialized.
      // We don't want to cause its initialization just for this check.
      val accountAuthItem = reconnectToSyncItem

      val menuItems = listOfNotNull(
//         toolbarItem,
         bookmarksItem,
         historyItem,
         downloadsItem,
         extensionsItem,
         syncSignInMenuItem,
         accountAuthItem,
         BrowserMenuDivider(),
         desktopItem,
         BrowserMenuDivider(),
         whatsNewItem,
         helpItem,
         customizeHomeItem,
         settingsItem,
         quitItem
      ).also { items ->
         items.getHighlight()?.let { onHighlightPresent(it) }
      }

      return menuItems
   }

   init {
      val menuItems = coreMenuItems()

      // Report initial state.
      onMenuBuilderChanged(BrowserMenuBuilder(menuItems, endOfMenuAlwaysVisible = true))
   }
}
