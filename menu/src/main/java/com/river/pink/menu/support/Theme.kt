package com.river.pink.menu.support

import android.content.Context
import android.content.res.Resources
import android.util.TypedValue
import androidx.annotation.AnyRes
import androidx.annotation.AttrRes
import androidx.annotation.ColorInt
import androidx.core.content.ContextCompat

/**
 * Resolves the resource ID corresponding to the given attribute.
 *
 * @sample
 * context.theme.resolveAttribute(R.attr.textColor) == R.color.light_text_color
 */
@AnyRes
fun Resources.Theme.resolveAttribute(@AttrRes attribute: Int): Int {
   val outValue = TypedValue()
   resolveAttribute(attribute, outValue, true)
   return outValue.resourceId
}

fun Context.resolveAttribute(attribute: Int): Int {
   val typedValue = TypedValue()
   theme.resolveAttribute(attribute, typedValue, true)
   return typedValue.resourceId
}

@ColorInt
fun Context.getColorFromAttr(@AttrRes attr: Int) =
   ContextCompat.getColor(this, theme.resolveAttribute(attr))
