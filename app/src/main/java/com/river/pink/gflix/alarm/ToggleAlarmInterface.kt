package com.river.pink.gflix.alarm

interface ToggleAlarmInterface {
   fun alarmToggled(id: Int, isEnabled: Boolean)
}