package com.river.pink.gflix.alarm

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.simplemobiletools.commons.helpers.ensureBackgroundThread

class HideAlarmReceiver : BroadcastReceiver() {
   override fun onReceive(context: Context, intent: Intent) {
      val id = intent.getIntExtra(ALARM_ID, -1)
      context.hideNotification(id)

      ensureBackgroundThread {
         val alarm = context.dbHelper.getAlarmWithId(id)
         if (alarm != null && alarm.days < 0) {
            context.dbHelper.updateAlarmEnabledState(alarm.id, false)
         }
      }
   }
}