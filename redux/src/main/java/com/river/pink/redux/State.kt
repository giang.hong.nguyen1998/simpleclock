package com.river.pink.redux

/**
 * Generic interface for a [State] maintained by a [Store].
 */
interface State