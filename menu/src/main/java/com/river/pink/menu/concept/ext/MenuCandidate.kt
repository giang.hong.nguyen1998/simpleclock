package com.river.pink.menu.concept.ext

import com.river.pink.menu.concept.candidate.CompoundMenuCandidate
import com.river.pink.menu.concept.candidate.DecorativeTextMenuCandidate
import com.river.pink.menu.concept.candidate.DividerMenuCandidate
import com.river.pink.menu.concept.candidate.DrawableMenuIcon
import com.river.pink.menu.concept.candidate.HighPriorityHighlightEffect
import com.river.pink.menu.concept.candidate.LowPriorityHighlightEffect
import com.river.pink.menu.concept.candidate.MenuCandidate
import com.river.pink.menu.concept.candidate.MenuEffect
import com.river.pink.menu.concept.candidate.MenuIcon
import com.river.pink.menu.concept.candidate.MenuIconEffect
import com.river.pink.menu.concept.candidate.NestedMenuCandidate
import com.river.pink.menu.concept.candidate.RowMenuCandidate
import com.river.pink.menu.concept.candidate.TextMenuCandidate

private fun MenuIcon?.effect(): MenuIconEffect? =
    if (this is DrawableMenuIcon) effect else null

/**
 * Find the effects used by the menu.
 * Disabled and invisible menu items are not included.
 */
fun List<MenuCandidate>.effects(): Sequence<MenuEffect> = this.asSequence()
    .filter { option -> option.containerStyle.isVisible && option.containerStyle.isEnabled }
    .flatMap { option ->
        when (option) {
            is TextMenuCandidate -> sequenceOf(option.effect, option.start.effect(), option.end.effect()).filterNotNull()
            is CompoundMenuCandidate -> sequenceOf(option.effect, option.start.effect()).filterNotNull()
            is NestedMenuCandidate -> sequenceOf(option.effect, option.start.effect(), option.end.effect()).filterNotNull() +
                    option.subMenuItems?.effects().orEmpty()
            is RowMenuCandidate -> option.items.asSequence()
                    .filter { it.containerStyle.isVisible && it.containerStyle.isEnabled }
                    .mapNotNull { it.icon.effect }
            is DecorativeTextMenuCandidate, is DividerMenuCandidate -> emptySequence()
        }
    }

/**
 * Find a [NestedMenuCandidate] in the list with a matching [id].
 */
fun List<MenuCandidate>.findNestedMenuCandidate(id: Int): NestedMenuCandidate? = this.asSequence()
    .mapNotNull { it as? NestedMenuCandidate }
    .find { it.id == id }

/**
 * Select the highlight with the highest priority.
 */
fun Sequence<MenuEffect>.max() = maxByOrNull {
    // Select the highlight with the highest priority
    when (it) {
        is HighPriorityHighlightEffect -> 2
        is LowPriorityHighlightEffect -> 1
    }
}
