package com.river.pink.redux.helpers

import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import com.river.pink.redux.Action
import com.river.pink.redux.State
import com.river.pink.redux.Store
import com.river.pink.redux.ext.flowScoped
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.Flow

/**
 * Helper class for creating small binding classes that are responsible for reacting to state
 * changes.
 */
@ExperimentalCoroutinesApi // Flow
abstract class AbstractBinding<in S : State>(
   private val store: Store<S, out Action>
) : DefaultLifecycleObserver {
   private var scope: CoroutineScope? = null

   override fun onStart(owner: LifecycleOwner) {
      scope = store.flowScoped { flow ->
         onState(flow)
      }
   }

   override fun onStop(owner: LifecycleOwner) {
      scope?.cancel()
   }

   /**
    * A callback that is invoked when a [Flow] on the [store] is available to use.
    */
   abstract suspend fun onState(flow: Flow<S>)
}